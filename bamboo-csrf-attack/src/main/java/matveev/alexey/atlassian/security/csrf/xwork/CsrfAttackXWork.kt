package matveev.alexey.atlassian.security.csrf.xwork

import com.atlassian.bamboo.ww2.BambooActionSupport
import matveev.alexey.atlassian.security.csrf.service.PluginSettingsService
import org.slf4j.LoggerFactory

class CsrfAttackXWork(private val pluginSettingsService: PluginSettingsService) : BambooActionSupport() {
    var formType = "XWork"
    var endpointUrl = "savecsrfxwork.action"

    private var parameter1 : String = "default value"


    override fun doDefault(): String {
        return INPUT
    }

    override fun execute(): String {
        pluginSettingsService.parameter1_webwork = this.parameter1
        return SUCCESS
    }

    fun getParameter1() : String {
        return pluginSettingsService.parameter1_webwork
    }

    fun setParameter1(value: String) {
        this.parameter1 = value
    }

    companion object {
        private val log = LoggerFactory.getLogger(CsrfAttackXWork::class.java)
    }
}
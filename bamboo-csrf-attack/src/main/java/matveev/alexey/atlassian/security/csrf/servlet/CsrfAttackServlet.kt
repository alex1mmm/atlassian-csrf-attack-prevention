package matveev.alexey.atlassian.security.csrf.servlet


import com.atlassian.templaterenderer.TemplateRenderer
import matveev.alexey.atlassian.security.csrf.service.PluginSettingsService
import org.slf4j.LoggerFactory
import java.io.IOException
import java.util.*
import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CsrfAttackServlet(val templateRenderer: TemplateRenderer,
                        val pluginSettingsService: PluginSettingsService
) : HttpServlet() {

    var parameter1 : String = "default value"
        get() = pluginSettingsService.parameter1_servlet


    @Throws(ServletException::class, IOException::class)
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        renderView(req, resp)
    }


    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        val atlToken = req.getParameter("servlet_atl_token")
        if (atlToken == null || getToken(req).isEmpty() || atlToken != getToken(req)
        ) {
            resp.sendError(403, "invalid xsrf token")
            return
        }
        val parameter1: String  = req.getParameter("parameter1")
        pluginSettingsService.parameter1_servlet = parameter1
        renderView(req, resp)
    }

    fun renderView(req: HttpServletRequest, resp: HttpServletResponse) {
        resp.contentType = "text/html"
        val context: MutableMap<String, Any> = mutableMapOf()
        context.put("endpointUrl", "csrfservlet")
        context.put("formType", "Servlet")
        context.put("servlet_atl_token", getToken(req))
        context.put("parameter1", this.parameter1)
        this.templateRenderer.render(
            "/templates/form.vm",
            context, resp.getWriter()
        )
    }

    private fun getToken(request: HttpServletRequest): String {
        val session = request.session
        var token: String? = session.getAttribute("servlet_atl_token")?.toString()
        if (token == null) {
            token = UUID.randomUUID().toString()
            session.setAttribute("servlet_atl_token", token)
        }
        return token
    }

    companion object {
        private val log = LoggerFactory.getLogger(CsrfAttackServlet::class.java)
    }
}
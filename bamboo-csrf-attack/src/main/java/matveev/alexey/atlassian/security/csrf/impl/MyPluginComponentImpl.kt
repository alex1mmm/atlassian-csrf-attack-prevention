package matveev.alexey.atlassian.security.csrf.impl

import com.atlassian.sal.api.ApplicationProperties
import matveev.alexey.atlassian.security.csrf.api.MyPluginComponent

class MyPluginComponentImpl(private val applicationProperties: ApplicationProperties?) :
    MyPluginComponent {
    override val name: String
        get() = if (null != applicationProperties) {
            "myComponent:" + applicationProperties.displayName
        } else "myComponent"

}
package matveev.alexey.atlassian.security.csrf.service

import com.atlassian.sal.api.pluginsettings.PluginSettings
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory

class PluginSettingsServiceImpl (pluginSettingsFactory: PluginSettingsFactory) :
    PluginSettingsService {
    val pluginSettings: PluginSettings

    override var parameter1_servlet: String
        get() = getSettingValue(PARAMETER1_SERVLET)
        set(value) {setSettingValue(PARAMETER1_SERVLET, value)}

    override var parameter1_webwork: String
        get() = getSettingValue(PARAMETER1_WEBWORK)
        set(value) {setSettingValue(PARAMETER1_WEBWORK, value)}

    private fun setSettingValue(settingKey: String, settingValue: String?) {
        pluginSettings.put(PLUGIN_STORAGE_KEY + settingKey, settingValue ?: "")
    }

    private fun getSettingValue(settingKey: String): String {
        return if (pluginSettings[PLUGIN_STORAGE_KEY + settingKey] != null) pluginSettings[PLUGIN_STORAGE_KEY + settingKey].toString() else ""
    }

    companion object {
        private const val PLUGIN_STORAGE_KEY = "matveev.alexey.atlassian.security.csrf"
        private const val PARAMETER1_SERVLET = "parameter1_servlet"
        private const val PARAMETER1_WEBWORK = "parameter1_webwork"

    }

    init {
        pluginSettings = pluginSettingsFactory.createGlobalSettings()
    }

}


package ut.matveev.alexey.atlassian.security.csrf;

import org.junit.Test;
import matveev.alexey.atlassian.security.csrf.api.MyPluginComponent;
import matveev.alexey.atlassian.security.csrf.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest {
    @Test
    public void testMyName() {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent", component.getName());
    }
}
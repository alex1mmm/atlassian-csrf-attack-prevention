package ut.matveev.alexey.atlassian.security.csrf.jira.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import matveev.alexey.atlassian.security.csrf.jira.jira.webwork.CsrfAttackWebwork;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class CsrfAttackWebworkTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CsrfAttackWebwork testClass = new CsrfAttackWebwork();

        throw new Exception("CsrfAttackWebwork has no tests!");

    }

}

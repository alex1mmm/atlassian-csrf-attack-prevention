package matveev.alexey.atlassian.security.csrf.jira.servlet

import com.atlassian.jira.security.xsrf.XsrfTokenGenerator
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor
import com.atlassian.sal.api.xsrf.XsrfTokenValidator
import com.atlassian.templaterenderer.TemplateRenderer
import matveev.alexey.atlassian.security.csrf.jira.service.PluginSettingsService
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class CsrfAttackServlet(val templateRenderer: TemplateRenderer,
                        val pluginSettingsService: PluginSettingsService,
                        val xsrfTokenGenerator: XsrfTokenGenerator,
                        val xsrfTokenAccessor: XsrfTokenAccessor,
                        val xsrfTokenValidator: XsrfTokenValidator
)                       : HttpServlet() {

    var parameter1 : String = "default value"
    get() = pluginSettingsService.parameter1_servlet


    @Throws(ServletException::class, IOException::class)
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        renderView(req, resp)
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        // You could use xsrfTokenValidator or do everything yourself
        // val validationBySAL = xsrfTokenValidator.validateFormEncodedToken(req)
        val atlToken: String? =  req.getParameter("atl_token")
        if (atlToken == null || atlToken != xsrfTokenGenerator.generateToken(req)) {
            resp.sendError(403, "wrong xsrf token")
            return
        }
        val parameter1: String  = req.getParameter("parameter1")
        pluginSettingsService.parameter1_servlet = parameter1
        renderView(req, resp)
    }

    fun renderView(req: HttpServletRequest, resp: HttpServletResponse) {
        val context: MutableMap<String, Any> = mutableMapOf()
        context.put("endpointUrl", "csrfservlet")
        context.put("formType", "Servlet")
        context.put("atl_token", xsrfTokenGenerator.generateToken(req))
        context.put("parameter1", this.parameter1)
        this.templateRenderer.render(
            "/templates/csrf-attack-webwork/csrfattackwebwork/form.vm",
            context, resp.getWriter()
        )
    }
}
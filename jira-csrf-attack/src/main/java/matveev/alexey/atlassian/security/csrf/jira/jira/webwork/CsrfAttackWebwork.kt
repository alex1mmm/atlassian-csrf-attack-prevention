package matveev.alexey.atlassian.security.csrf.jira.jira.webwork

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck
import com.atlassian.jira.web.action.JiraWebActionSupport
import org.slf4j.LoggerFactory
import matveev.alexey.atlassian.security.csrf.jira.service.PluginSettingsService


class CsrfAttackWebwork(val pluginSettingsService: PluginSettingsService) : JiraWebActionSupport() {

    val endpointUrl = "CsrfAttackWebwork!save.jspa"
    val formType = "Webwork"

    private var parameter1: String = "default value"

    override fun setErrorMessages(p0: MutableCollection<String>?) = TODO()

    override fun doExecute(): String {
        return SUCCESS
    }

    @RequiresXsrfCheck
    fun doSave(): String {
        pluginSettingsService.parameter1_webwork = this.parameter1
        return SUCCESS
    }

    fun getParameter1(): String {
        return pluginSettingsService.parameter1_webwork
    }

    fun setParameter1(value: String) {
        this.parameter1 = value
    }

    companion object {
        private val log = LoggerFactory.getLogger(CsrfAttackWebwork::class.java)
    }
}

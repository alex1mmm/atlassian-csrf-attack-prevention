package matveev.alexey.atlassian.security.csrf.jira.impl

import com.atlassian.sal.api.ApplicationProperties
import matveev.alexey.atlassian.security.csrf.jira.api.MyPluginComponent


class MyPluginComponentImpl(private val applicationProperties: ApplicationProperties?) : MyPluginComponent {
    override fun getName(): String {
        return if (null != applicationProperties) {
            "myComponent:" + applicationProperties.displayName
        } else "myComponent"
    }
}
package matveev.alexey.atlassian.security.csrf.jira.api

interface MyPluginComponent {
    fun getName() : String
}
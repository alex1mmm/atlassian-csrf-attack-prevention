package matveev.alexey.atlassian.security.csrf.jira.config

import com.atlassian.jira.security.xsrf.XsrfTokenGenerator
import com.atlassian.plugins.osgi.javaconfig.OsgiServices
import com.atlassian.plugins.osgi.javaconfig.configs.beans.ModuleFactoryBean
import com.atlassian.plugins.osgi.javaconfig.configs.beans.PluginAccessorBean
import com.atlassian.sal.api.ApplicationProperties
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor
import com.atlassian.sal.api.xsrf.XsrfTokenValidator
import com.atlassian.templaterenderer.TemplateRenderer
import matveev.alexey.atlassian.security.csrf.jira.api.MyPluginComponent
import matveev.alexey.atlassian.security.csrf.jira.impl.MyPluginComponentImpl
import org.osgi.framework.ServiceRegistration
import org.springframework.beans.factory.FactoryBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import matveev.alexey.atlassian.security.csrf.jira.service.PluginSettingsService
import matveev.alexey.atlassian.security.csrf.jira.service.PluginSettingsServiceImpl


@Configuration
@Import(ModuleFactoryBean::class, PluginAccessorBean::class)
open class MyPluginJavaConfig {
    // imports ApplicationProperties from OSGi
    @Bean
    open fun xsrfTokenGenerator() : XsrfTokenGenerator {
        return OsgiServices.importOsgiService(XsrfTokenGenerator::class.java)
    }
    @Bean
    open fun xsrfTokenValidator(): XsrfTokenValidator {
        return OsgiServices.importOsgiService(XsrfTokenValidator::class.java)
    }

    @Bean
    open fun xsrfTokenAccessor(): XsrfTokenAccessor {
        return OsgiServices.importOsgiService(XsrfTokenAccessor::class.java)
    }

    @Bean
    open fun pluginSettingsFactory(): PluginSettingsFactory {
        return OsgiServices.importOsgiService(PluginSettingsFactory::class.java)
    }
    @Bean
    open fun templateRenderer(): TemplateRenderer {
        return OsgiServices.importOsgiService(TemplateRenderer::class.java)
    }

    @Bean
    open fun applicationProperties(): ApplicationProperties {
        return OsgiServices.importOsgiService(ApplicationProperties::class.java)
    }

    @Bean
    open fun pluginSettingsService(): PluginSettingsService {
        return PluginSettingsServiceImpl(pluginSettingsFactory())
    }
    @Bean
    open fun myPluginComponent(applicationProperties: ApplicationProperties?): MyPluginComponent {
        return MyPluginComponentImpl(applicationProperties)
    }

    // Exports MyPluginComponent as an OSGi matveev.alexey.atlassian.security.csrf.jira.service
    @Bean
    open fun registerMyDelegatingService(
            mypluginComponent: MyPluginComponent?): FactoryBean<ServiceRegistration<*>> {
        return OsgiServices.exportOsgiService(mypluginComponent, null, MyPluginComponent::class.java)
    }
}
package matveev.alexey.atlassian.security.csrf.confluence.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.xwork.SimpleXsrfTokenGenerator;
import matveev.alexey.atlassian.security.csrf.confluence.service.PluginSettingsService;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Named
public class CsrfAttackServlet extends HttpServlet {
    private final TemplateRenderer templateRenderer;
    private final PluginSettingsService pluginSettingsService;
    private final SimpleXsrfTokenGenerator simpleXsrfTokenGenerator;

    @Inject
    public CsrfAttackServlet(@ComponentImport TemplateRenderer templateRenderer,
                             PluginSettingsService pluginSettingsService
    ) {
        this.templateRenderer = templateRenderer;
        this.simpleXsrfTokenGenerator = new SimpleXsrfTokenGenerator();
        this.pluginSettingsService = pluginSettingsService;
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        this.renderView(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String atlToken = req.getParameter("servlet_atl_token");
        if (atlToken == null || simpleXsrfTokenGenerator.generateToken(req) == null || !atlToken.equals(simpleXsrfTokenGenerator.generateToken(req))) {
            resp.sendError(403, "invalid xsrf token");
            return;
        }
        String parameter1 = req.getParameter("parameter1");
        pluginSettingsService.setParameter1Servlet(parameter1);
        this.renderView(req, resp);
    }

    private void  renderView(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        Map<String, Object> context = new HashMap<>();
        context.put("formType", "Servlet");
        context.put("endpointUrl", "csrfservlet");
        context.put("servlet_atl_token", simpleXsrfTokenGenerator.generateToken(req));
        context.put("parameter1", this.pluginSettingsService.getParameter1Servlet());
        this.templateRenderer.render(
                "/templates/form.vm",
                context,
                resp.getWriter()
        );
    }
}


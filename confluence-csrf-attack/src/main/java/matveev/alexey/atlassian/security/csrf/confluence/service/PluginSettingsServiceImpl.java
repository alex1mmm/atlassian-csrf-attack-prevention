package matveev.alexey.atlassian.security.csrf.confluence.service;


import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PluginSettingsServiceImpl implements PluginSettingsService {

    public final PluginSettings pluginSettings;
    private static final String PLUGIN_STORAGE_KEY = "matveev.alexey.atlassian.security.csrf";
    private static final String PARAMETER1_SERVLET = "parameter1_servlet";
    private static final String PARAMETER1_WEBWORK = "parameter1_webwork";

    @Inject
    public PluginSettingsServiceImpl(@ComponentImport PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();

    }

    private void setSettingValue(String settingKey, String settingValue) {
        this.pluginSettings.put(PLUGIN_STORAGE_KEY + settingKey, settingValue != null?settingValue:"");
    }

    private String getSettingValue(String settingKey) {
        return pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey) != null?pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey).toString():"";
    }


    @Override
    public String getParameter1Servlet() {
        return getSettingValue(PARAMETER1_SERVLET);
    }

    @Override
    public void setParameter1Servlet(String value) {
        setSettingValue(PARAMETER1_SERVLET, value);
    }

    @Override
    public String getParameter1Webwork() {
        return getSettingValue(PARAMETER1_WEBWORK);
    }

    @Override
    public void setParameter1Webwork(String value) {
        setSettingValue(PARAMETER1_WEBWORK, value);
    }


}

package matveev.alexey.atlassian.security.csrf.confluence.xwork;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.xwork.RequireSecurityToken;
import matveev.alexey.atlassian.security.csrf.confluence.service.PluginSettingsService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class CsrfAttackXWork extends ConfluenceActionSupport {
    private static String formType = "XWork";
    private static String endpointUrl = "savecsrfxwork.action";
    private final PluginSettingsService pluginSettingsService;
    private String parameter1  = "default value";

    @Inject
    public CsrfAttackXWork(PluginSettingsService pluginSettingsService) {
        this.pluginSettingsService = pluginSettingsService;
    }

    @Override
     public String doDefault() {
        return INPUT;
    }

    @Override
    public String execute(){
        pluginSettingsService.setParameter1Webwork(this.parameter1);
        return SUCCESS;
    }

    public String getParameter1() {
        return pluginSettingsService.getParameter1Webwork();
    }

    public void setParameter1(String value) {
        parameter1 = value;
    }

    public String getFormType() {
        return formType;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

}

package matveev.alexey.atlassian.security.csrf.confluence.service;

public interface PluginSettingsService {
    String getParameter1Servlet();
    void setParameter1Servlet(String value);
    String getParameter1Webwork();
    void setParameter1Webwork(String value);
}

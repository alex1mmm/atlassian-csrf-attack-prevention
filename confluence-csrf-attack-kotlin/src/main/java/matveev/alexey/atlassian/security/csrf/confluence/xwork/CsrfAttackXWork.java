package matveev.alexey.atlassian.security.csrf.confluence.xwork;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import matveev.alexey.atlassian.security.csrf.confluence.service.PluginSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CsrfAttackXWork extends ConfluenceActionSupport {
    private static String formType = "XWork";
    private static String endpointUrl = "savecsrfxwork.action";
    private final PluginSettingsService pluginSettingsService;
    private String parameter1  = "default value";

    @Autowired
    public CsrfAttackXWork(PluginSettingsService pluginSettingsService) {
        this.pluginSettingsService = pluginSettingsService;
    }

    @Override
     public String doDefault() {
        return INPUT;
    }

    @Override
    public String execute(){
        //    pluginSettingsService.parameter1_webwork = this.parameter1
        return SUCCESS;
    }

    public String getParameter1() {
        return "hello";
        //    return pluginSettingsService.parameter1_webwork
    }

    public void setParameter1(String value) {
        parameter1 = value;
    }

}

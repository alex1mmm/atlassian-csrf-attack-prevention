package ut.matveev.alexey.atlassian.security.csrf

import matveev.alexey.atlassian.security.csrf.confluence.api.MyPluginComponent
import matveev.alexey.atlassian.security.csrf.confluence.impl.MyPluginComponentImpl
import org.junit.Assert
import org.junit.Test

class MyComponentUnitTest {
    @Test
    fun testMyName() {
        val component: MyPluginComponent = MyPluginComponentImpl(null)
        Assert.assertEquals("names do not match!", "myComponent", component.name)
    }
}
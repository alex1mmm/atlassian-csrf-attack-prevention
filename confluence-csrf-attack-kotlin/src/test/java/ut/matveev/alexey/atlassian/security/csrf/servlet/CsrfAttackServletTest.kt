package ut.matveev.alexey.atlassian.security.csrf.servlet

import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CsrfAttackServletTest {
    var mockRequest: HttpServletRequest? = null
    var mockResponse: HttpServletResponse? = null
    @Before
    fun setup() {
        mockRequest = Mockito.mock(HttpServletRequest::class.java)
        mockResponse = Mockito.mock(HttpServletResponse::class.java)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testSomething() {
        val expected = "test"
        Mockito.`when`(mockRequest!!.getParameter(Mockito.anyString())).thenReturn(expected)
        Assert.assertEquals(expected, mockRequest!!.getParameter("some string"))
    }
}
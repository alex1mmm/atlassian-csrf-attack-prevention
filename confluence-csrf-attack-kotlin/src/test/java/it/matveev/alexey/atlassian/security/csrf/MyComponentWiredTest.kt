package it.matveev.alexey.atlassian.security.csrf

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner
import com.atlassian.sal.api.ApplicationProperties
import matveev.alexey.atlassian.security.csrf.confluence.api.MyPluginComponent
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AtlassianPluginsTestRunner::class)
class MyComponentWiredTest(
    private val applicationProperties: ApplicationProperties,
    private val myPluginComponent: MyPluginComponent
) {
    @Test
    fun testMyName() {
        Assert.assertEquals(
            "names do not match!",
            "myComponent:" + applicationProperties.displayName,
            myPluginComponent.name
        )
    }
}
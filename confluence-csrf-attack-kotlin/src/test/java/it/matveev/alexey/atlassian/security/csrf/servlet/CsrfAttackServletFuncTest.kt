package it.matveev.alexey.atlassian.security.csrf.servlet

import org.apache.http.client.HttpClient
import org.apache.http.client.ResponseHandler
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.impl.client.DefaultHttpClient
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class CsrfAttackServletFuncTest {
    var httpClient: HttpClient? = null
    var baseUrl: String? = null
    var servletUrl: String? = null
    @Before
    fun setup() {
        httpClient = DefaultHttpClient()
        baseUrl = System.getProperty("baseurl")
        servletUrl = "$baseUrl/plugins/servlet/csrfservlet"
    }

    @After
    fun tearDown() {
        httpClient!!.connectionManager.shutdown()
    }

    @Test
    @Throws(IOException::class)
    fun testSomething() {
        val httpget = HttpGet(servletUrl)

        // Create a response handler
        val responseHandler: ResponseHandler<String> = BasicResponseHandler()
        val responseBody = httpClient!!.execute(httpget, responseHandler)
        Assert.assertTrue(null != responseBody && "" != responseBody)
    }
}
package matveev.alexey.atlassian.security.csrf.service;

public interface PluginSettingsService {
    String getParameter1Servlet();
    void setParameter1Servlet(String value);
}

package matveev.alexey.atlassian.security.csrf.service;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class PluginSettingsServiceImpl implements PluginSettingsService {

    public final PluginSettings pluginSettings;
    private static final String PLUGIN_STORAGE_KEY = "matveev.alexey.atlassian.security.csrf";
    private static final String PARAMETER1_SERVLET = "parameter1_servlet";

    public PluginSettingsServiceImpl(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();

    }

    private void setSettingValue(String settingKey, String settingValue) {
        this.pluginSettings.put(PLUGIN_STORAGE_KEY + settingKey, settingValue != null?settingValue:"");
    }

    private String getSettingValue(String settingKey) {
        return pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey) != null?pluginSettings.get(PLUGIN_STORAGE_KEY + settingKey).toString():"";
    }


    @Override
    public String getParameter1Servlet() {
        return getSettingValue(PARAMETER1_SERVLET);
    }

    @Override
    public void setParameter1Servlet(String value) {
        setSettingValue(PARAMETER1_SERVLET, value);
    }


}

package matveev.alexey.atlassian.security.csrf.servlet;

import com.atlassian.fisheye.plugin.web.RequiresXsrfCheck;
import com.atlassian.templaterenderer.TemplateRenderer;
import matveev.alexey.atlassian.security.csrf.service.PluginSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CsrfAttackServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(CsrfAttackServlet.class);
    private final TemplateRenderer templateRenderer;
    private final PluginSettingsService pluginSettingsService;

    public CsrfAttackServlet(TemplateRenderer templateRenderer,
                             PluginSettingsService pluginSettingsService) {
        this.templateRenderer = templateRenderer;
        this.pluginSettingsService = pluginSettingsService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        this.renderView(resp);
    }

    @Override
    @RequiresXsrfCheck
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String parameter1 = req.getParameter("parameter1");
        pluginSettingsService.setParameter1Servlet(parameter1);
        this.renderView(resp);
    }

    private void  renderView(HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        Map<String, Object> context = new HashMap<>();
        context.put("endpointUrl", "csrfservlet");
        context.put("parameter1", this.pluginSettingsService.getParameter1Servlet());
        this.templateRenderer.render(
                "/templates/form.vm",
                context,
                resp.getWriter()
        );
    }

}
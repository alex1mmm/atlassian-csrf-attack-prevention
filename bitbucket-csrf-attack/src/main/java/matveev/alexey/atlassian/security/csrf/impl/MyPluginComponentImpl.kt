package matveev.alexey.atlassian.security.csrf.impl

import matveev.alexey.atlassian.security.csrf.api.MyPluginComponent

class MyPluginComponentImpl(applicationProperties: com.atlassian.sal.api.ApplicationProperties?) :
    MyPluginComponent {
    private val applicationProperties: com.atlassian.sal.api.ApplicationProperties?
    override val name: String
        get() = if (null != applicationProperties) {
            "myComponent:" + applicationProperties.getDisplayName()
        } else "myComponent"

    init {
        this.applicationProperties = applicationProperties
    }
}
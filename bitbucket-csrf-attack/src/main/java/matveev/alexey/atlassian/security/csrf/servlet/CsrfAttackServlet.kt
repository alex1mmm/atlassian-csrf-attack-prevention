package matveev.alexey.atlassian.security.csrf.servlet


import com.atlassian.sal.api.xsrf.XsrfTokenAccessor
import com.atlassian.sal.api.xsrf.XsrfTokenValidator
import com.atlassian.templaterenderer.TemplateRenderer
import matveev.alexey.atlassian.security.csrf.service.PluginSettingsService
import org.slf4j.LoggerFactory
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CsrfAttackServlet(val templateRenderer: TemplateRenderer,
                        val xsrfTokenAccessor: XsrfTokenAccessor,
                        val xsrfTokenValidator: XsrfTokenValidator,
                        val pluginSettingsService: PluginSettingsService
) : HttpServlet() {

    var parameter1 : String = "default value"
        get() = pluginSettingsService.parameter1_servlet


    @Throws(ServletException::class, IOException::class)
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        renderView(req, resp)
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        if (!xsrfTokenValidator.validateFormEncodedToken(req)) {
            resp.sendError(403, "invalid xsrf token")
            return
        }
        val parameter1: String  = req.getParameter("parameter1")
        pluginSettingsService.parameter1_servlet = parameter1
        renderView(req, resp)
    }

    fun renderView(req: HttpServletRequest, resp: HttpServletResponse) {
        resp.contentType = "text/html"
        val context: MutableMap<String, Any> = mutableMapOf()
        context.put("endpointUrl", "csrfservlet")
        context.put("formType", "Servlet")
        context.put("atl_token", xsrfTokenAccessor.getXsrfToken(req, resp, true))
        context.put("parameter1", this.parameter1)
        this.templateRenderer.render(
            "/templates/form.vm",
            context, resp.getWriter()
        )
    }

    companion object {
        private val log = LoggerFactory.getLogger(CsrfAttackServlet::class.java)
    }
}